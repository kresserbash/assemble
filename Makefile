GAME = cogs-of-war

debug = -gdwarf
src = ./src
build = ./build

FLAGS = $(debug)

SYSTEM = main.o

OBJECTS = part.o engine.o fuel.o

$(GAME) : $(SYSTEM)
	cc -o $(build)/$(GAME) $(build)/part.o $(build)/engine.o $(build)/fuel.o $(build)/main.o

main.o : $(OBJECTS)
	cc $(debug) -c $(src)/main.c -o $(build)/main.o

part.o :
	cc $(debug) -c $(src)/part.c -o $(build)/part.o

engine.o :
	cc $(debug) -c $(src)/engine.c -o $(build)/engine.o

fuel.o :
	cc $(debug) -c $(src)/fuel.c -o $(build)/fuel.o

clean :
	rm -r $(build)/*
