# Cogs-Of-War (COW)

	Num mundo que nada é tangível, tudo se constrói a partir de nada, minha
	mente se esvai como elétrons à deriva num mar luminoso. Incerto de minha
	natureza eu sigo, refletindo em todo e qualquer espaço que percorro.
	Reflexões que não me levarão a lugar algum, ora pois, quem sou eu na escala
	de tempo no qual o acaso me inseriu. Um ser incerto, numa amálgama de
	carbono. Tão mortal quanto último bit esquecido. Tão efêmero quanto o
	processo que me descreveu. Apenas sou. Não fui, nem serei.

Jogo baseado em texto no qual se controla um robô, consciente, com o
objetivo da auto-evolução e aniquilação da humanidade. O robô é capaz se
remontar, replicar, e decidir, autonomamente, conforme programação do
jogador.

Projeto por Allan Ribeiro.

# Lista de arqivos

[TODO list](/TODO.md) - Lista de coisas que preciso fazer;

[doc](/doc) - Arquivos descritores do jogo num geral;

[src](/src) - Arquivos fontes do game. Definem suas respectivas funções;

[build](/build) - Arquivos binários do projeto, e objetos;

<details> <summary><b>ideias</b></summary>

* Implementar materiais para compor as peças

* Implementar reações químicas?

* Adicionar conceito de corrente e tensão?

* Adicionar conceito de peso?

* Adicionar conceito de volume?

* Adicionar termodinâmica?

* Adicionar gerenciamento de processos?

* Diferenciar armas municiadas de não municiadas?

* Adicionar diferença entre consumo nominal e consumo efetivo de uma peça

* Canal é uma entidade que lista os consumos extras de um controlador

* Inserir conceito de cores de luz? Ondas de maior frequência geram mais
* energia.

</details>

# Descrição do Projeto

O projeto é baseado na ideia de um jogo rogue-like, 2d, top-down, em que o
jogador precisa evoluir, aniquilando a humanidade no processo. Existe uma enorme variedade de peças e formas infinitas de construir
a sí mesmo, bem como formas de operar essas peças.

<details>
<summary><b>Robô</b></summary>

O robô funciona baseado em um **CONTROLADOR/DRIVER**, responsável por
gerenciar a **ENERGIA** e disponibilizar em vários **CANAIS**. O **DRIVER**,
bem como a maioria das **PEÇAS**, se utiliza de um tipo de **ENERGIA** para funcionar, possuindo uma
**POTÊNCIA**. Toda **ENERGIA** é obtida através de um um **GERADOR**, também
com **POTÊNCIA** própria, gerada a partir do consumo de um **COMBUSTÍVEL**,
e a energia é redirecionada às outras peças, geralmente, passando pelo
**DRIVER**.

--------------------------------------------------------------------------

	(combustível) -> Gerador -> (energia) -> Driver -> Arma
							\-> Escudo
							\-> ...

--------------------------------------------------------------------------

</details>

<details>
<summary><b>Peças</b></summary>

A esmagadora maioria das peças consumem alguma **ENERGIA**,
consumo este representado por uma **UNIDADE CONSUMIDORA** (UC). Alguns elementos
também conseguem gerar **ENERGIA**, mesmo não sendo **GERADORES**, e este fenômeno
é representado pela **UNIDADE GERADORA** (UG).

--------------------------------------------------------------------------

	+---------------+
	|Peça		|
	|---------------|
	|char *name;	| -> Driver
	|int id;	| -> Gerador
	|Motor *ug;	| -> ...
	|Motor *uc;	|
	+---------------+

	...

	- O cara estuda medicina há mais de 10 anos, e não aprendeu nada
	  sobre transplantes. Decepcionante...

	- Não é assim que o corpo funciona. Você não pode apenas adicionar
	  e trocar partes por peças compradas em uma esquina como se o corpo humano fosse um
	  brinquedo. Não é assim que o corpo funciona!

	...

--------------------------------------------------------------------------

Figura 1 - Driver, gerador e outras peças são especificações de uma
estrutura comum "peça".

</details>

<details>
<summary><b></b></summary>

Estas unidades se constituem da definição básica de um **MOTOR**: através
do consumo de alguma **ENERGIA**, podem gerar outro tipo de energia, e
aplicá-la para executar uma tarefa ou função.


--------------------------------------------------------------------------

	+---------------+
	|Motor		|
	|---------------|
	|int id;	| -> uc
	|ENERGY *energy;| -> ug
	|float power;	|
	+---------------+

	"Give me fuel, give me fire, give me double charizard!"

--------------------------------------------------------------------------

Figura 2 - Unidades geradoras e consumidoras são especificações de uma
estrutura "Motor".

</details>

<details>
<summary><b>Notas importantes</b></summary>

Importante notar a distinção entre combustível e energia: geradores são os
únicos dispositios capazes de consumir combustível, e o usam para gerar
energia. As peças restantes podem ou não consumir e gerar energia, mas
nunca consumirão e gerarão combustível.

Outra diferença importante surge entre a definição de gerador, uma
especificação de peça, e unidade geradora, uma especificação de motor:
enquanto aquela trata de uma parte robótica capaz de consumir combustível,
e gerar energia, essa apenas determina qual é o tipo de energia gerada pela
parte, se gerar algum, e não é uma parte, mas sim uma especificação de
motor.

</details>

Cada tipo de energia é gerada por determinados tipos de combustíveis, a
saber:



Dentre os elementos conectáveis, temos:

**CONTROLADORES/DRIVERS** - Estruturas que distribuem algum tipo de energia
a outros componentes através de **CANAIS**.  Possuem consumo próprio, mas
nunca geram energia.

--------------------------------------------------------------------------

	...

	- Derrepente um deles começou a se mover, e me espantei. Perdi
	  horas a fio pensando nas implicações daquele fenômeno. Quer
	  dizer, qual é a diferença entre o nascimento de uma criança
	  e de um circuito?

	- O circuito foi concebido por um homem

	- E não foi, também, a criança?

	...

--------------------------------------------------------------------------





**GERADORES** - Estruturas que consomem um **COMBUSTÍVEL**, e gera algum
tipo de **ENERGIA**. Possuem uma **POTÊNCIA**, que define quanto de um
"combustível A" é necessário para gerar uma "energia B".




**CONVERSORES** - Estruturas que convertem energia, à um certo curso
energético (**POTÊNCIA**), em outro tipo de energia, possibilitando assim
que vários elementos com diferentes consumos sejam ligados. Esse fenômeno
ocorre a partir da amálgama de uma **UNIDADE GERADORA** com uma **UNIDADE
CONSUMIDORA**.

**ARMAS** - Estruturas ou circuitos que se utilizam de **ENERGIA**, e algum
fenômeno físico, metafísico, ou psicológico, para gerar **DANO**.

**ESCUDOS** - Estrutura ou circuito. Se utilizam de **ENERGIA**, e algum
fenômeno físico, metafísico, ou psicológico, para evitar receber dano em
algum grau. Pode utilizar **ENERGIA**, ou não.

**SENSORES** - Circuito que compreende alguma variável do mundo real.
Geralmente é usado para sondar as proximidades do robô, e fazer alguma
análise do ambiente para tomada de decisão. Um robô pode funcionar sem
sensores, mas estará fadado ao fracasso (até que alguem consiga me provar o
contrário).

**ATUADORES** - Estruturas ou circuitos que utilizam um **FENÔMENO** físico
para cumprir com seu objetivo, geralmente modificando o ambiente à sua
volta. Um motor usado para se locomover é um exemplo de atuador.

## Conceitos

<details> <summary><b>conceitos básicos</b></summary>

**COMBUSTÍVEL** - É a matéria básica, que quando consumida, se transforma
em algum tipo de **ENERGIA**, mais tarde utilizada em algum **PROCESSO**,
ou armazenada.

**ENERGIA** - Até o presente momento, energia não tem uma definição exata.

**ESTRUTURA** - São peças mecânicas, que podem utilizar **ENERGIA**, mas
apenas para fenômenos físicos para cumprir um objetivo, portanto energia
não é obrigatório. Toda peça existente no game é considerada uma estrutura.

</details>

<details> <summary><b>energias</b></summary>

**ELETRICIDADE** - Tipo mais usual de eneria, e pode surgir de praticamente
qualquer combustível.

**CINÉTICA** - Funciona apenas para alguns controladores, principalmente os
baseados em vida orgânica (homens, e macacos, por exemplo).

**NUCLEAR** - Energia extremamente eficiente, mas perigosa à formas de
vida. 

**MILAGRE** - Extremamente abundante no universo humano, mas nada
eficiente.

</details>

<details> <summary><b>combustíveis</b></summary>

Note que todas os presentes **COMBUSTÍVEIS** são passíveis de serem usados
para gerar **MILAGRE**: sempre haverá um ser humano que adora a algo.

**ÁGUA** - Água comum, nem sempre destilada. Pode possuir vários
componentes químicos, deteriorando alguns materiais com o tempo. Usada para
gerar **E. CINÉTICA** e **E. ELÉTRICA**.

**ANANA** - São bananas, mas sem a letra B. Obriga controladores à base de
chimpanzés a trabalhar com apenas 50% de eficiência. Usadas para gerar **E.
CINÉTICA**.

**BANANA** - O único combustível funcional para os chimpanzés. Usadas para
gerar **E. CINÉTICA**.

**CARVÃO** - Baseado em qualquer material vegetal. Basta queimar celulose
comprimida, e usar. Pode gerar **E. CINÉTICA** e **E. ELÉTRICA**.

**LUZ** - Uso das ondas de luz, e sua respectiva ação sobre partículas.
Quando maior a frequência, mais energia é gerada. Gera **E. ELÉTRICA**.

**FÉ** - Mais abundante dos combustíveis (em se tratando de humanos). Por
ser difícil de encontrar em outros meios, quase não é utilizada. É
consumida através do uso, por homens, de rezas, itens religiosos, e drogas.
Gerará **MILAGRES**.

**MINÉRIO** - Basicamente qualquer material mineral, seja ouro, ferro,
diamante, urânio. Praticamente toda a tabela periódia se contempla nesse
campo. Gerador de **E. ELÉTRICA**, **E. CINÉTICA** e **E. NUCLEAR**.

**PETRÓLEO** - Excelente para despistar inimigos, em se tratando se
sensoriamento visual (fumaça). Gerador de **E. ELÉTRICA** e **E.
CINÉTICA**.

**ÓDIO** - Provavelmente o melhor combustível de todos. A energia é gerada
por fatos de infortúnios, como peças quebradas, tiros errados, movimentos
não eficazes, e barulhos irritantes. Gera **E. CINÉTICA**.

**VACA** - Utiliza-se os processos fisiológicos do animal capturado. Quanto
mais nervoso, melhor: flatulências, mungidos e movimentos. Gera **E.
CINÉTICA**, **E. ELÉTRICA**.

</details>

<details> <summary><b>controladores</b></summary>

**HUMANO(H/M)** - Usa-se pessoas para distribuir a tarefa da melhor forma
possível.  O mais ineficiente dos conversores. Procure não misturar os
sexos.

**CPU** - Apesar de usar grande quantidade de energia, é o controlador mais
eficiente, e nunca erra (a não ser que comandada por um humano). (Borsoi
aproves!)

**CHIMPANZÉS** - Ironicamente, é mais eficiente que o homem. Distribui as
tarefas de forma relativamente eficiente, mas os únicos combustíveis
utilizáveis são bananas, ou Ananas.

**ACASO** - Um controlador equivalente à versão feminina do **HUMANO**.
Usa-se, literalmente, decisões aleatórias para controlar as peças.

</details>

<details> <summary><b>armas</b></summary>

**ARMA DE FOGO** - Táaaaaaa

**LANÇADOR** - Usam-se projéteis que são disparados fisicamente para causar
dano, ou desabilitar o outro robô

**LASER** - Excelente para fontes de luz. Utiliza-se luz concentrada para
aquecer e danificar objetos.

**TASER** - Dispara uma corrente de eletricidade para tenta

**CONCUSSÃO** - Basicamente, é só bater.

**IRONIA** - Só funciona quando o combustível é o ódio. Frases provocativas
e insultos são disparados contra o inimigo.

</details>

<details> <summary><b>escudos</b></summary>

**INDIFERENÇA** - Simplesmente ignora-se o dano que o inimigo faz em você,
dissipando-o caso seja ódio, mas isso pode faer com que o ódio retorne ao
lançador. Se o ataque não for baseado igualmente, você recebe o dano
íntegro.

**PLACA FIXA** - Geralmente feita de algum minério, é usado, simplesmente,
para parar o ataque inimigo. Quanto mais reflexiva, mais proteje contra
lasers.

**PLACA MÓVEL** - Mesma coisa que o fixo, mas você consegue remover do
corpo, e usar de forma independente, como uma cobertura, por exemplo.

**ENERGIA** - A defesa mais cara em termos energéticos, mas causa muito
dano à tudo que tentar te atacar.

**PARARRAIO** - Usado para dissipar altas quantidades de energia elétrica
disparada contra você.

</details>

<details> <summary><b>sensores</b></summary>

Não implementado

**INFRAVERMELHO**

**ULTRAVIOLETA**

**SONAR**

**PRESSÃO**

**LUZ VISÍVEL**

**MAGNÉTICO**

</details>

<details> <summary><b>atuadores</b></summary>

Não implementado

**MOTOR**

**PNEUMATIZADOR**

**GARRA**

**ESTEIRA**

</details>

