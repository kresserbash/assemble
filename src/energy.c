#include "energy.h"

int initEnergy(ENERGY *energy)
{
	if(!energy)
	{
		return NOPAR;
	}

	energy->name = NULL;
	return 0;
}

int defineEnergy(ENERGY *energy, char *newName)
{
	if(!(energy && newName))
	{
		return NOPAR;
	}

	energy->name = (char *)malloc(20*sizeof(char));
	if (!energy->name)
	{
		return NOMEM;
	}
	strcpy(energy->name, newName);
	return 0;
}

int endEnergy(ENERGY *energy)
{
	if (!energy)
	{
		return NOPAR;
	}

	int n = strlen(energy->name);
	for (int i = 0; i < n; i++)
	{
		energy->name[i] = 0;
	}
	free(energy->name);
	free(energy);
	return 0;
}
