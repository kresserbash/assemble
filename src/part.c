/*

Definições das peças do jogo

// TODO:

// [ ] Criar uma lib para definir macros de erros: OK(0), NOPAR(100),
// NOMEM(200): cada sinal de erro tem até 100 possibilidades de
// especificação: 102 significa que o terceiro item envolvido em uma
// declaração de parâmetros não foi devidamente alocado: param == NULL,
// representado por NOPAR+2;

// 0,OK - bão dimais
// 100,NOPAR - Algum dos parâmetros é NULL
// 200,NOMEM - Algo que devia ser alocado não foi, provavelmente por falta de
// memória;
// 300,STRER - Algum elemento interno de uma struct deveria existir, mas
// não existe

// [ ] Criar macro para limitar tamanho dos nomes, e torná-la global
// (substituir nos malloc(20*sizeof()), no 20)

// MANUEL:
//

// NOME
//

// SINOPSE
//

// DESCRIÇÃO
//

*/

#include "part.h"

int initPart(PART *part)
{
	if (part == NULL)
	{
		return NOPAR;
	}

	part->id = 0;
	part->name = NULL;
	part->consumer = NULL;
	part->generator = NULL;

	return 0;
}

int definePart(PART *part, int newId, char *newName, ENGINE *newConsumer, ENGINE *newGenerator)
{
	if (!(part && newName && newConsumer && newGenerator)) //Algum destes parâmetros não existem?
	{
		return NOPAR;
	}

	part->id = newId;
	part->generator = newGenerator;
	part->consumer = newConsumer;

	part->name = (char *) malloc(20*sizeof(char));
	if (!part->name)
	{
		return NOMEM;
	}
	strcpy(part->name, newName);
	return 0;
}

int endPart(PART *part)
{
	if (!part)
	{
		return NOPAR;
	}
	part->id = 0;
	if(part->generator || part->consumer)
	{
		return NFREE;
	}

	int n = strlen(part->name);
	for (int i = 0; i < n; i++)
	{
		part->name[i] = 0;
	}
	free(part->name);
	free(part);
	return 0;
}
