#ifndef LIBPART
#define LIBPART

#include <stdlib.h>
#include <string.h>
#include "error.h"
#include "part.h"

#define CONVERSOR struct conversor

CONVERSOR
{
	PART *core;
};

int initConversor(CONVERSOR *);
int defineConversor(CONVERSOR *, int, char *, ENGINE *, ENGINE *);
int endConversor(PART *);

#endif
