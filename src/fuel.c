#include "fuel.h"

int initFuel(FUEL *fuel)
{
	if(!fuel)
	{
		return NOPAR;
	}

	fuel->name = NULL;
	fuel->eficiency = 0;
	fuel->density = 0;
	return 0;
}

int defineFuel(FUEL *fuel, char *newName, float newEficiency, float newDensity)
{
	if(!(fuel && newName))
	{
		return NOPAR;
	}

	fuel->name = (char *)malloc(20*sizeof(char));
	if (!fuel->name)
	{
		return NOMEM;
	}
	strcpy(fuel->name, newName);

	fuel->density = newDensity;
	fuel->eficiency = newEficiency;

	return 0;
}

int endFuel(FUEL *fuel)
{
	if (!fuel)
	{
		return NOPAR;
	}

	int n = strlen(fuel->name);
	for (int i = 0; i < n; i++)
	{
		fuel->name[i] = 0;
	}
	free(fuel->name);

	fuel->density = fuel->eficiency = 0;

	free(fuel);
	return 0;
}
