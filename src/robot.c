/*
// TODO:
// [ ] Criar header do arquivo
//
// [ ] Criar função de inicialização
//
// [ ] Criar função de free
//
// [ ] Definir função de geração aleatória
//
// [ ] Definir função de geração procedural
//
// [ ] Definir função de geração seletiva
//
//
*/

#include "robot.h"

int initRobot(void)
{

}

int defineRobot(void)
{
	PART *novaParte = (PART *)malloc(sizeof(PART));
	CONSUMER *cu = (CONSUMER *) malloc(sizeof(CONSUMER));
	GENERATOR *gu = (GENERATOR *) malloc(sizeof(GENERATOR));

	initPart(novaParte);
	initGenerator(gu);
	initConsumer(cu);

	char *nome = (char *)malloc(20*sizeof(char));
	scanf(" %s", nome);

	char *fuel = (char *)malloc(20*sizeof(char));
	scanf(" %s", fuel);

	defineConsumer(cu, 1, fuel, 10.1);
	defineGenerator(gu, 2, fuel, 10.2);
	definePart(novaParte, 3, nome, cu, gu);

	return 0;
}

endRobot()
{
	
}
