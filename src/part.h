#ifndef LIBPART
#define LIBPART

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"
#include "engine.h"

#define PART struct part

PART
{
	int id;
	char *name;
	ENGINE *consumer, *generator;
};

int initPart(PART *);
int definePart(PART *, int, char *, ENGINE *, ENGINE *);
int endPart(PART *);

#endif
