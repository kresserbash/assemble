#ifndef LIBERROR
#define LIBERROR

//Erros de memória

//#define OK 0 //Tudo certo com o processo
#define NOPAR 1 //Um dos parâmetros que a função recebeu não existe
#define NOMEM 2 //Algum malloc falhou
#define STERR 3 //Alguma struct tem um membro inexistente
#define NFREE 4 //Algum free deveria ser feito antes de algum procedimento, e não foi

//Erros de conversores

#define EQFUEL 100 //O conversor converte um combustível pra ele mesmo

#endif
