/*
// TODO:
//
// [ ] - Tem como incluir o header sem usar include aqui?
//
//
*/

#include "engine.h"

int initEngine(ENGINE *engine)
{
	if (!engine) //Vefica se o ponteiro é NULL
	{
		return NOPAR;
	}

	engine->id = 0;
	engine->energy = NULL;
	engine->power = 0;

	return 0;
}

int defineEngine(ENGINE *engine, int newId, FUEL *newEnergy, float newPower, char consumer)
{

	if (!(engine && newEnergy)) //engine ou newEnergy não existem(NULL)?
	{
		return NOPAR;
	}
	engine->id = newId;
	engine->energy = newEnergy;

	if (consumer)
	{
		if(newPower < 0)
		{
			engine->power = newPower;
		}
		else
		{
			engine->power = -newPower;
		}
	}
	else
	{
		if(newPower > 0)
		{
			engine->power = newPower;
		}
		else
		{
			engine->power = -newPower;
		}
	}
	return 0;
}

int endEngine(ENGINE *engine)
{
	if (!engine)
	{
		return NOPAR;
	}

	engine->id = engine->power = 0;

	if (engine->energy) //Se retornar algo diferente de 0, energy ainda existe, e o free n pode ocorrer
	{
		return NFREE;
	}
	free(engine);
	return 0;

}

