#ifndef LIBFUEL
#define LIBFUEL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"

#define FUEL struct fuel

FUEL
{
	char *name;
	float eficiency, density;
};

int initFuel(FUEL *);
int defineFuel(FUEL *, char *, float, float);
int endFuel(FUEL *);

#endif
