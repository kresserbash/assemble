/* conversor.c

// TODO:
//
// [ ] 1: Resolver (1) de peças, em TODO.md;
//
// [ ] 2: Criar função getModel(), que nomeia um conversor baseado nas
// características da peça;
//
//
//


// MANUEL:
//

// NOME
//	conversor.c - Estruturas para parte conversora de energias de um
//	robô
//

// SINOPSE
//	#include <conversor.h>
//
//	int initConversor(struct conversor *c);
//	int defineConversor(struct conversor *c, ...);
//
//
// DESCRIÇÃO
//

*/

#include "conversor.h"

int initConversor(CONVERSOR *conversor)
{
	if (conversor == NULL)
	{
		return NOPAR;
	}

	conversor->core = (PART *)malloc(sizeof(PART));

	return initPart(conversor->core);
}

int defineConversor(CONVERSOR *conversor, ENGINE *newConsumer, ENGINE *newGenerator)
{
	if (!(conversor && newConsumer && newGenerator)) //Algum destes parâmetros não existem?
	{
		return NOPAR;
	}

	if (newConsumer->fuel == newGenerator->fuel)
	{
		return EQFUEL;
	}

	definePart(newConsumer, newGenerator);
	getModel(conversor);
	return 0;
}

int endPart(PART *part)
{
	if (!part)
	{
		return NOPAR;
	}
	part->id = 0;
	if(part->generator || part->consumer)
	{
		return NFREE;
	}

	int n = strlen(part->name);
	for (int i = 0; i < n; i++)
	{
		part->name[i] = 0;
	}
	free(part->name);
	free(part);
	return 0;
}

/*
// Minha rinite está me atacando
*/
